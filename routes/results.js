const express = require('express');
const router = express.Router();
const usersController = require('../controllers/result/resultsController');

// /users
router.get('/', usersController.list);

router.post('/', usersController.create);

router.put('/', usersController.update);

router.delete('/', usersController.destroy);

module.exports = router;

const express = require('express');

function list(req, res, next){
  let n1 = req.params.n1;
  let n2 = req.params.n2;
  let r=n1+n2;
  res.json(r);
}

function create(req, res, next){
  let n1 = req.params.n1;
  let n2 = req.params.n2;
  let r=n1*n2;
  res.json(r);
}

function update(req, res, next){
  let n1 = req.params.n1;
  let n2 = req.params.n2;
  let r=n1/n2;
  res.json(r);
}

function destroy(req, res, next){
  let n1 = req.params.n1;
  let n2 = req.params.n2;
  let r=n1-n2;
  res.json(r);
}

module.exports = {
  index, list, create, update, destroy
}

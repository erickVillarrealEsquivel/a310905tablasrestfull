const express = require('express');

function list(req, res, next){
  let users = [];
  res.json(users);
}

function index(req, res, next){
  let id = req.params.id;
  console.log(`El id de usuario es ${id}`);
  let user = {};
  res.json(user);
}

function create(req, res, next){
  let name = req.body.name;
  res.render('index', {title: `Se creo un elemento de nombre ${name}`});
}

function update(req, res, next){
  res.render('index', {title:'se actualizo un elemento'});
}

function destroy(req, res, next){
  res.render('index', {title:'elimino un elemento'});
}

module.exports = {
  index, list, create, update, destroy
}
